package sk.svkit.gatewayservice;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class SimpleSourceBean {

    private final Source source;

    public void publishSimpleEvent() {
        SimpleEvent simpleEvent = new SimpleEvent(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "simpleAction");

        source.output().send(MessageBuilder.withPayload(simpleEvent).build());
    }
}
