package sk.svkit.gatewayservice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class SimpleEvent {

    private String id;

    private String correlationId;

    private String action;
}
