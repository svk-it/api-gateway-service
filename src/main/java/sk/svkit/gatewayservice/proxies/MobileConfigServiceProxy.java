package sk.svkit.gatewayservice.proxies;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class MobileConfigServiceProxy {

    private final WebClient.Builder lbWebClientBuilder;

    private final ReactorLoadBalancerExchangeFilterFunction lbFilterFunction;

    public Mono<Integer> getNumberOfCalls() {
        Mono<ClientResponse> response = lbWebClientBuilder.build()
                .get()
                .uri("http://callme-service/stats")
                .exchange();

        return response.flatMap(resp -> {
            switch (resp.statusCode()) {
                case OK:
                    return resp.bodyToMono(Integer.class);
                case NOT_FOUND:
                    return Mono.error(new RuntimeException("Not found"));
                default:
                    return Mono.error(new RuntimeException("Unknown" + resp.statusCode()));
            }
        });
    }
}
