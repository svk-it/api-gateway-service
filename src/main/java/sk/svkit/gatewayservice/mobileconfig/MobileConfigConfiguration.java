package sk.svkit.gatewayservice.mobileconfig;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

@Configuration
@LoadBalancerClient(name = "callme-service")
public class MobileConfigConfiguration {

    @Bean
    public RouteLocator mobileConfigProxyRouting(RouteLocatorBuilder builder) {
        return builder.routes().
                route("callme_service_route", r -> r.path("/sync")
                        .filters(f -> f.rewritePath("/sync", "/stats"))
                        .uri("lb://callme-service"))
                .build();
    }

    @Bean
    public RouterFunction<ServerResponse> mobileConfigHandler(MobileConfigHandlers mobileConfigHandlers) {
        return RouterFunctions.route(GET("/async"), mobileConfigHandlers::getNumberOfCalls);
    }

    @Bean
    @LoadBalanced
    public WebClient.Builder webClientBuilder() {
        return WebClient.builder();
    }
}
