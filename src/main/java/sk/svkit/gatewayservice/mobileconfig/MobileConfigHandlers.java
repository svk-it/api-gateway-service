package sk.svkit.gatewayservice.mobileconfig;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Mono;
import sk.svkit.gatewayservice.SimpleSourceBean;
import sk.svkit.gatewayservice.proxies.MobileConfigServiceProxy;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@Component
@RequiredArgsConstructor
public class MobileConfigHandlers {

    private final MobileConfigServiceProxy mobileConfigServiceProxy;

    private final SimpleSourceBean simpleSourceBean;

    public Mono<ServerResponse> getNumberOfCalls(ServerRequest serverRequest) {

//        return mobileConfigServiceProxy.getNumberOfCalls().flatMap(noc -> ServerResponse.ok()
//                .contentType(MediaType.APPLICATION_JSON)
//                .body(fromObject(noc)))
//                .onErrorResume(RuntimeException.class, e -> ServerResponse.notFound().build());

        simpleSourceBean.publishSimpleEvent();

        Mono<ServerResponse> serverResponse = ServerResponse.accepted().contentType(MediaType.APPLICATION_JSON).build();

        return serverResponse;
    }
}
